#!/usr/bin/env python
# -*- coding: utf-8 -*-

#!******************************************************************************!
#! Copyright (C) 2014-2024 OpenLoops Collaboration. For authors see authors.txt !
#!                                                                              !
#! This file is part of OpenLoops.                                              !
#!                                                                              !
#! OpenLoops is free software: you can redistribute it and/or modify            !
#! it under the terms of the GNU General Public License as published by         !
#! the Free Software Foundation, either version 3 of the License, or            !
#! (at your option) any later version.                                          !
#!                                                                              !
#! OpenLoops is distributed in the hope that it will be useful,                 !
#! but WITHOUT ANY WARRANTY; without even the implied warranty of               !
#! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                !
#! GNU General Public License for more details.                                 !
#!                                                                              !
#! You should have received a copy of the GNU General Public License            !
#! along with OpenLoops.  If not, see <http://www.gnu.org/licenses/>.           !
#!******************************************************************************!

from __future__ import print_function

import sys
import argparse
import time
import openloops
import parallel

# =============== #
# argument parser #
# =============== #

parser = argparse.ArgumentParser()
parser.add_argument(
    '--vers', '--version', dest='version', action='store_true', default=False,
    help='''Print OpenLoops version.''')

# =============== #
# option handling #
# =============== #


stroptions = openloops.config['pyrunoptions']
stroptions.extend([arg for arg in sys.argv[1:]
                 if ('=' in arg and not arg.startswith('-'))])
args = parser.parse_args(
    [arg for arg in sys.argv[1:] if (arg.startswith('-') or '=' not in arg)])

if args.version:
  print(openloops.config['release'])



